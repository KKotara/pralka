package ZadanieZaliczeniowe;

public class Amica extends Pralka {
	
	public Amica(int program, double temp, int V) throws myException {
		super(program, temp, V);
		this.mark = type.Amica;
	}
	
	public void showStatus(Amica pralka) {
		System.out.println("Marka: " + pralka.getMark() + ", numer programu: " + pralka.getProgram() + ", temperatura: " + pralka.getTemp() + " ºC, " + "obroty: "
				+ pralka.getV() + "obr/min");
	}
	
}
