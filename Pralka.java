package ZadanieZaliczeniowe;

public abstract class Pralka {
	
	protected int		program;
	protected int		maxprogram	= 20;
	protected double	changetemp	= 0.5;
	protected double	temp;
	private int			V;
	protected type		mark;
	
	enum type {
		Whirpool,
		Amica,
		Beko;
	}
	
	public Pralka(int program, double temp, int V) throws myException {
		if (program < 0 || program > maxprogram) {
			throw new myException("Program spoza zakresu");
		} else {
			setProgram(program);
		}
		if (temp < 0 || temp > 90) {
			throw new myException("Temperatura spoza zakresu");
		} else {
			setTemp(temp);
		}
		if (V < 0 || V > 1000) {
			throw new myException("Obroty spoza zakresu");
		} else {
			setV(V);
		}
	}
	
	public type getMark() {
		return mark;
	}
	
	public void setMark(type marka) {
		this.mark = marka;
	}
	
	public int getProgram() {
		return program;
	}
	
	public void setProgram(int program) {
		this.program = program;
	}
	
	public void nextProgram() {
		if (program == maxprogram) {
			program = 1;
		} else {
			program++;
		}
	}
	
	public void previousProgram() {
		if (program == 1) {
			program = maxprogram;
		} else {
			program--;
		}
	}
	
	public double getTemp() {
		return temp;
	}
	
	public void setTemp(double temp) {
		this.temp = Math.round(temp * 2.0) / 2.0;
	}
	
	public void tempUp() throws myException {
		if (temp < 90 && temp >= 0) {
			temp += changetemp;
			System.out.println("Current temp: " + temp + "ºC");
		} else {
			throw new myException("Osiągnięto temp. maksymalną");
		}
	}
	
	public void tempDown() throws myException {
		if (temp <= 90 && temp > 0) {
			temp -= changetemp;
			System.out.println("Current temp: " + temp + "ºC");
		} else {
			throw new myException("Osiągnięto temp. minimalną");
		}
	}
	
	public int getV() {
		return V;
	}
	
	public void setV(int v) {
		this.V = Math.round(v / 100) * 100;
	}
	
	public void VUp() {
		if (V < 1000 && V >= 0) {
			V += 100;
		} else {
			V = 0;
		}
	}
	
	public void VDown() {
		if (V <= 1000 && V > 0) {
			V -= 100;
		} else {
			V = 1000;
		}
	}
	
	public void showStatus(Pralka pralka) {
		System.out.println("Numer programu: " + pralka.getProgram() + ", temperatura: " + pralka.getTemp() + " ºC, " + "obroty: "
				+ pralka.getV() + "obr/min");
	}
	
}
