package ZadanieZaliczeniowe;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.stream.Collectors;

public class Main {
	
	public static void washingA(Amica x) throws myException {
		x.showStatus(x);
		x.nextProgram();
		x.showStatus(x);
		x.previousProgram();
		x.showStatus(x);
		try {
			x.tempDown();
		} catch (myException my) {
			System.out.println(my.getMessage());
		}
		try {
			x.tempUp();
		} catch (myException my) {
			System.out.println(my.getMessage());
		}
		x.VDown();
		x.showStatus(x);
		x.VUp();
		x.showStatus(x);
	}
	
	public static void washingB(Beko y) throws myException {
		y.showStatus(y);
		y.nextProgram();
		y.showStatus(y);
		y.previousProgram();
		y.showStatus(y);
		try {
			y.tempUp();
		} catch (myException my) {
			System.out.println(my.getMessage());
		}
		try {
			y.tempDown();
		} catch (myException my) {
			System.out.println(my.getMessage());
		}
		y.VDown();
		y.showStatus(y);
		y.VUp();
		y.showStatus(y);
	}
	
	public static void washingC(Whirpool z) throws myException {
		z.showStatus(z);
		z.nextProgram();
		z.showStatus(z);
		z.previousProgram();
		z.showStatus(z);
		try {
			z.tempDown();
		} catch (myException my) {
			System.out.println(my.getMessage());
		}
		try {
			z.tempUp();
		} catch (myException my) {
			System.out.println(my.getMessage());
		}
		z.VDown();
		z.showStatus(z);
		z.VUp();
		z.showStatus(z);
	}
	
	public static void sort(Amica x, Beko y, Whirpool z) {
		ArrayList<Pralka> all = new ArrayList<Pralka>();
		all.add(x);
		all.add(z);
		all.add(y);
		
		Comparator<Pralka> sortComparator = (a, b) -> a.mark.name()
				.compareTo(b.mark.name());
		System.out.println("Przed sortowaniem:");
		all.stream()
				.forEach(d -> System.out.println("[Marka: " + d.getMark() + ", numer programu: " + d.getProgram() + ", temperatura: " + d.getTemp() + " ºC, " + "obroty: "
						+ d.getV() + "obr/min]"));
		System.out.println("Po sortowaniu: ");
		all.stream()
				.sorted(sortComparator)
				.collect(Collectors.toList())
				.forEach(e -> System.out.println("[Marka: " + e.getMark() + ", numer programu: " + e.getProgram() + ", temperatura: " + e.getTemp() + " ºC, " + "obroty: "
						+ e.getV() + "obr/min]"));
	}
	
	public static void main(String[] args) throws myException {
		
		try {
			Amica x = new Amica(1, 74.68, 20);
			Beko y = new Beko(10, 88, 520);
			Whirpool z = new Whirpool(22, 0.2, 920);
			washingA(x);
			washingB(y);
			washingC(z);
			sort(x, y, z);
		} catch (myException my) {
			System.out.println(my.getMessage());
		} finally {
			
		}
	}
	
}
