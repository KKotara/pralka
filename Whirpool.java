package ZadanieZaliczeniowe;

public class Whirpool extends Pralka {
	
	public Whirpool(int program, double temp, int V) throws myException {
		super(0, temp, V);
		maxprogram = 25;
		if (program < 0 || program > maxprogram) {
			throw new myException("Program spoza zakresu");
		} else {
			setProgram(program);
		}
		this.mark = type.Whirpool;
	}
	
	public void nextProgram() {
		maxprogram = 25;
		super.nextProgram();
	}
	
	public void previousProgram() {
		maxprogram = 25;
		super.previousProgram();
	}
	
	public void showStatus(Whirpool pralka) {
		System.out.println("Marka: " + pralka.getMark() + ", numer programu: " + pralka.getProgram() + ", temperatura: " + pralka.getTemp() + " ºC, " + "obroty: "
				+ pralka.getV() + "obr/min");
	}
}
