package ZadanieZaliczeniowe;

public class Beko extends Pralka {
	
	public Beko(int program, double temp, int V) throws myException {
		super(program, temp, V);
		this.mark = type.Beko;
		
	}
	
	public double getTemp() {
		return temp;
	}
	
	public void setTemp(double temp) {
		this.temp = Math.round(temp);
	}
	
	public void tempUp() throws myException {
		changetemp = 1;
		super.tempUp();
	}
	
	public void tempDown() throws myException {
		changetemp = 1;
		super.tempDown();
	}
	
	public void showStatus(Beko pralka) {
		System.out.println("Marka: " + pralka.getMark() + ", numer programu: " + pralka.getProgram() + ", temperatura: " + pralka.getTemp() + " ºC, " + "obroty: "
				+ pralka.getV() + "obr/min");
	}
	
}
